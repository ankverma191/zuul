package com.app.zuul.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.app.zuul.filters.PreFilter;
import com.app.zuul.filters.RouteFilter;

@Configuration
public class ZuulConfig {
	
	@Bean
	public PreFilter preFilter() {
		return new PreFilter();
	}
	
	@Bean
	public RouteFilter routeFilter() {
		return new RouteFilter();
	}

}
